#include <iostream>
#include <thread>
#include <mutex>
std::mutex foo,bar;

void task_a (int number_of_steps) {
  while(number_of_steps--) {
    std::lock (foo, bar);
    std::cout << "task a" <<std::endl;
    bar.unlock(); foo.unlock();
  }
}

void task_b (int number_of_steps) {
  while(number_of_steps--) {
    std::lock (bar, foo);
    std::cout << "task b" <<std::endl;
    bar.unlock(); foo.unlock();
  }
}

int main () {
  const int number_of_steps = 100;
  std::thread th1 (task_a, number_of_steps);
  std::thread th2 (task_b, number_of_steps);

  th1.join();
  th2.join();
  return 0;
}
