// http://www.cplusplus.com/reference/mutex/lock/
#include <iostream>
#include <thread>
#include <mutex>
std::mutex foo,bar;

void task_a (int number_of_steps) {
  while(number_of_steps--) {
    foo.lock(); bar.lock();
    std::cout << "task a" <<std::endl;
    foo.unlock(); bar.unlock();
  }
}

void task_b (int number_of_steps) {
  while(number_of_steps--) {
    bar.lock(); foo.lock();
    std::cout << "task b" <<std::endl;
    bar.unlock(); foo.unlock();
  }
}

int main () {
  const int number_of_steps = 100;
  std::thread th1 (task_a, number_of_steps);
  std::thread th2 (task_b, number_of_steps);

  th1.join();
  th2.join();
  return 0;
}
