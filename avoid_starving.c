// http://www.bogotobogo.com/cplusplus/multithreaded.php
#include <stdio.h>
#include <pthread.h>

static volatile int balance = 0;
pthread_mutex_t myMutex;

void *deposit(void *param) {
    char *who = (char*)param;

    int i;
    printf("%s: begin\n", who);
    for (i = 0; i < 1000000; i++) {
        pthread_mutex_lock(&myMutex);
        balance = balance + 1;
        pthread_mutex_unlock(&myMutex);
    }
    printf("%s: done\n", who);
    return NULL;
}

int main() {
    pthread_t p1, p2;
    char a[] = "A";
    char b[] = "B";
    printf("main() starts depositing, balance = %d\n", balance);
    pthread_create(&p1, NULL, deposit, a);
    pthread_create(&p2, NULL, deposit, b);

    pthread_join(p1, NULL);
    pthread_join(p2, NULL);
    printf("main() A and B finished, balance = %d\n", balance);
    return 0;
}
