CC = gcc
CXX = g++
CFLAGS = -g -Wall
TARGET1 = deadlock
TARGET2 = avoid_deadlock
TARGET3 = starving
TARGET4 = avoid_starving

%.o: %.cpp $(DEPS)
	$(CXX) -std=c++11 -c -o $@ $< $(CFLAGS)
	
%.o: %.c $(DEPS)
	$(CXX) -c -o $@ $< $(CFLAGS)

.PHONY: compile

compile: deadlock avoid_deadlock avoid_deadlock2 starving avoid_starving

deadlock: deadlock.o
	$(CXX) -pthread -o $(TARGET1) deadlock.o
	
avoid_deadlock: avoid_deadlock.o
	$(CXX) -pthread -o $(TARGET2) avoid_deadlock.o
	
avoid_deadlock2: avoid_deadlock2.o
	$(CXX) -pthread -o $(TARGET2)2 avoid_deadlock2.o

starving: starving.o
	$(CC)  -pthread -o $(TARGET3) starving.o

avoid_starving: avoid_starving.o
	$(CC)  -pthread -o $(TARGET4) avoid_starving.o

clean:
	rm -fR *.o
	rm -fR $(TARGET1)
	rm -fR $(TARGET2)
	rm -fR $(TARGET3)
	rm -fR $(TARGET4)
	rm -fR *.*~
	rm -fR *~
